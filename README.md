# bikerentalplatform

API for the Chicago Divvy Bike Rental platform using the Divvy API and the provided trip data.

Restore db using command "mongorestore -d practice-node-dev <path-to-db-folder-in-attachment>"
2. Go to the root directory of the attached project
3. From the terminal, run "npm install"
4. Once the packages are installed, run "npm run dev"
5. Import postman collection from "postman collection" folder from the attached project
6. Check all the endpoints