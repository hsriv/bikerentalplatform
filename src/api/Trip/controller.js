import moment from 'moment'
import Trip from './model'
import { getRidersById } from '../Rider/controller'

export const getTrips = async (req, res, next) => {
    try {
        const trips = await Trip.find()
        res.send({ error: false, trips: trips.map(trip => trip.view()) })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}


export const createTrip = async (req, res, next) => {
    try {
        const trip = await Trip.create(req.body)
        res.send({ error: false, trip: trip.view() })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}

const getRidersCountByAge = (riders) => {
    // [0-20,21-30,31-40,41-50,51+, unknown]
    const ridersByAgeGroups = {
        "0-20": 0,
        "21-30": 0,
        "31-40": 0,
        "41-50": 0,
        "51+": 0,
        "unknown": 0
    }
    for (let rider of riders) {
        if (rider.age === null || rider.age === undefined || rider.age === "") {
            ridersByAgeGroups["unknown"] += 1
        }
        if (rider.age > 0 && rider.age <= 20) {
            ridersByAgeGroups["0-20"] += 1
        }
        if (rider.age >= 21 && rider.age <= 30) {
            ridersByAgeGroups["21-30"] += 1
        }
        if (rider.age >= 31 && rider.age <= 40) {
            ridersByAgeGroups["31-40"] += 1
        }
        if (rider.age >= 41 && rider.age <= 50) {
            ridersByAgeGroups["41-50"] += 1
        }
        if (rider.age >= 51) {
            ridersByAgeGroups["51+"] += 1
        }
    }
    return ridersByAgeGroups
}

export const getRideCountByStation = async(req, res, next) => {
    try {
        if (req.body.activeStations.length === 0) {
            return res.send({ error: false, message: 'No rides available for the given stations' })
        }  
        if (!req.body.endTime) {
            return res.send({ error: true, message: 'Please provide a valid end date' })
        }
        const start = moment(req.body.endTime).startOf('day')
        const end = moment(req.body.endTime).endOf('day')
        const trips = await Trip.find({ destinationStationId: req.body.activeStations, endTime: { $gte: start, $lte: end } })
        const riders = await getRidersById(trips.map(trip => trip.riderId))
        res.send({ error: false, riders: getRidersCountByAge(riders), ridersAgesForReference: riders.map(rider => ({ riderId: rider.riderId, age: rider.age }))  })
    
        } catch (error) {
        res.send({ error: true, message: error })
    }
}

export const getTripsByDestinationId = (isPassOnly = false) => async(req, res, next) => {
    try {
        const trips = await Trip.find({ destinationStationId: String(req.params.destId) })
        const formattedTrips = trips.map(trip => trip.view())
        if (isPassOnly) {
            req.body.trips = formattedTrips
            return next()
        }
        res.send({ error: false, trips: formattedTrips })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}

export const getLatestTripByStationId = async (req, res, next) => {
    try {
        const start = moment(req.body.endTime).startOf('day')
        const end = moment(req.body.endTime).endOf('day')
        const tripPromises = req.body.activeStations.map(station => {
            console.log(station)
            return Trip.find({ destinationStationId: station, endTime: { $gte: start, $lte: end } }).sort('-endTime').limit(Number(req.query.count))
        })
        const trips = await Promise.all(tripPromises) 
        res.send({ error: false, trips: req.body.activeStations.map((station, index) =>  ({[station]: Object.values(trips[index]).map(trip => trip.view())})) })
    } catch (error) {
        console.log(error)
        res.send({ error: true, message: error })
    }
}