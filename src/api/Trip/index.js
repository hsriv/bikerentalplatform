import { Router } from 'express'
import { getTrips, createTrip, getRideCountByStation } from './controller'
import { getActiveStationsById } from '../Station/controller'
import { token } from '../../services/token'

const router = new Router()

router.get('/', token, getTrips)

router.post('/', token, createTrip)

router.post('/end', token, getActiveStationsById(true), getRideCountByStation)

export default router