import mongoose, { Schema } from 'mongoose'

// {
//     riderId : string,
//     originatingStationId : string,
//     destinationStationId : string,
//     startTime:Date
//     endTime: Date
//     }

const tripSchema = Schema({
    riderId: {
        type: String,
        required: true,
    },
    originatingStationId: {
        type: String,
        required: true,
    },
    destinationStationId: {
        type: String,
        required: true,
    },
    startTime: {
        type: Date,
        required: true
    },
    endTime: {
        type: Date,
        required: true
    },
}, {
    timestamps: true
})

tripSchema.methods = {
    view() {
        return {
            id: this.id,
            riderId: this.riderId,
            originatingStationId: this.originatingStationId,
            destinationStationId: this.destinationStationId,
            startTime: this.startTime,
            endTime: this.endTime,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt
        }
    }
}

const model = mongoose.model('Trip', tripSchema)

export default model