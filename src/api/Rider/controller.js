import Rider from './model'

export const getRiders = async (req, res, next) => {
    try {
        const riders = await Rider.find()
        res.send({ error: false, riders: riders.map(rider => rider.view()) })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}


export const createRider = async (req, res, next) => {
    try {
        const rider = await Rider.create(req.body)
        res.send({ error: false, rider: rider.view() })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}

export const getRidersById = async (riderIds, otherConditions = {}) => {
    const riders = await Rider.find({
        riderId: riderIds,
        ...otherConditions
    })
    return riders.map(rider => rider.view())
}