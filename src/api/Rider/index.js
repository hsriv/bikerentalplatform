import { Router } from 'express'
import { getRiders, createRider } from './controller'
import { token } from '../../services/token'

const router = new Router()

router.get('/', token, getRiders)

router.post('/', token, createRider)

export default router