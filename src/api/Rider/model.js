import mongoose, { Schema } from 'mongoose'

const riderSchema = Schema({
    riderId: {
        type: Number,
        required: true,
        unique: true
    },
    FirstName: {
        type: String,
        required: true,
    },
    LastName: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
    },
    PhoneNumber: {
        type: Number,
        required: true,
        unique: true
    }
}, {
    timestamps: true
})

riderSchema.methods = {
    view() {
        return {
            id: this.id,
            riderId: this.riderId,
            FirstName: this.FirstName,
            LastName: this.LastName,
            age: this.age,
            PhoneNumber: this.PhoneNumber,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt
        }
    }
}

const model = mongoose.model('Rider', riderSchema)

export default model