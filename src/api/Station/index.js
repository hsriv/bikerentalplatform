import { Router } from 'express'
import { getStations, createStation, getStationById, getRideInfo, getActiveStationsById } from './controller'
import { getTripsByDestinationId, getLatestTripByStationId } from '../Trip/controller'
import { token } from '../../services/token'

const router = new Router()

router.get('/', token, getStations)

router.post('/', token, createStation)

router.get('/stationId/:id', token, getStationById)

router.get('/stationId/:destId/rides', token, getTripsByDestinationId(true), getRideInfo)

router.post('/stationId/trips', token, getActiveStationsById(true), getLatestTripByStationId)

export default router