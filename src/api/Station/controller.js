import Station from './model'
import { getRidersById } from '../Rider/controller'

export const getStations = async (req, res, next) => {
    try {
        const stations = await Station.find()
        res.send({ error: false, stations: stations.map(station => station.view()) })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}


export const createStation = async (req, res, next) => {
    try {
        const station = await Station.create(req.body)
        res.send({ error: false, station: station.view() })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}

export const getStationById = async (req, res) => {
    
   if (req.params.id) {
    return Station.findOne({ stationId : req.params.id, isActive: true }).then(stationModel => {
        if (stationModel) return stationModel.view()
        return null
    }).then((station) => {
        if (!station) {
            return res.send({ error: true, message: 'Either the station is inactive or it does not exist' })
        }
        return res.send({ error: false, station })
    }).catch(error => {
        return res.send({ error: true, message: error })
    })
   }
    // try {
    //     if (req.params.id) {
    //         const station = await Station.findOne({ stationId : req.params.id, isActive: true })
    //         if (!station) {
    //             return res.send({ error: true, message: 'Either the station is inactive or it does not exist' })
    //         }
    //         return res.send({ error: false, station: station.view() })
    //     }
    //     return res.send({ error: true, station: null, message: 'Invalid request' })
    // } catch (error) {
    //     res.send({ error: true, message: error })
    // }
}

export const getActiveStationsById = (isPassOnly = false) => async (req, res, next) => {
    try {
        const stations = await Station.find({ stationId: req.body.stationIds, isActive: true })
        if (isPassOnly) {
            req.body.activeStations = stations.map(station => station.stationId)
            return next()
        }
        return res.send({ error: false, stations: stations.map(station => station.view()) })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}

export const getRideInfo = async (req, res, next) => {
    try {
        const riders = await getRidersById(req.body.trips.map(trip => trip.riderId), { age: { $gte: req.query.fromAge, $lte: req.query.toAge } })
        const riderIds = riders.map(rider => String(rider.riderId))
        const trips = req.body.trips.filter(trip => riderIds.includes(trip.riderId))
        res.send({ error: false, trips, ridersAgesForReference: riders.map(rider => ({ riderId: rider.riderId, age: rider.age })) })
    } catch (error) {
        res.send({ error: true, message: error })
    }
}