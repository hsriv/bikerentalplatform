import request from 'supertest'
import app from '../../app'


describe('Get station by id', () => {
  it('should respond with station details', async () => {
    const res = await request(app)
        .get('/stations/stationId/1234')
        .set('authorization', '6b9e30736f2c741ee02c431dc1cf68b0')
        .expect('Content-Type', /json/)
        .expect(200)
  })
})