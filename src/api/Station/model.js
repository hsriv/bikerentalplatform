import mongoose, { Schema } from 'mongoose'
    
const stationSchema = Schema({
    stationId: {
        type: Number,
        required: true,
        unique: true
    },
    stationName: {
        type: String,
        required: true,
        unique: true
    },
    isActive: {
        type: Boolean,
        required: true,
        default: true
    }
}, {
    timestamps: true
})

stationSchema.methods = {
    view() {
        return {
            id: this.id,
            stationId: this.stationId,
            stationName: this.stationName,
            isActive: this.isActive,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt
        }
    }
}

const model = mongoose.model('Station', stationSchema)

export default model