export const token = (req, res, next) => {
    if (req.headers.authorization) {
        return next()
    }
    return res.status(401).end()
}